#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>
#include <SRHook.hpp>

class AsiPlugin : public SRDescent {
	static constexpr std::string_view kConfigEntry = "scale=";

	SRHook::Hook<> scene{ 0x53EACE, 5 };
	SRHook::Hook<> endRender{ 0x53EBEE, 6 };
	SRHook::Hook<> beginRender2D{ 0x53EACE, 5 };
	SRHook::Hook<> beginRenderMenu{ 0x53EB19, 5 };
	SRHook::Hook<> endRender2D{ 0x53EBE4, 5 };
	SRHook::Hook<> sampTextDraws{ 0xA2077, 5, "samp" };

	size_t svid;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	void setViewport( const D3DVIEWPORT9 *vp );
	void sceneHook();
	void endRenderHook();
	void begin2D();
	void end2D();
	void updateTextures();
	void makeSmallTexture( LPDIRECT3DSURFACE9 &surface );
	void makeNormalTexture( LPDIRECT3DSURFACE9 &surface );

	bool changeSize_ = false;
	SRTexture *txSmall_ = nullptr;
	SRTexture *txNormal_ = nullptr;

	float scale_ = 0.75f;
};

#endif // MAIN_H
