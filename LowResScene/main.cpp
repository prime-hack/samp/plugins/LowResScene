#include "main.h"
#include <fstream>
#include <texture.h>
#include <gtasa.hpp>
#include <samp.hpp>

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	// Constructor
	std::ifstream cfg( PROJECT_NAME_C ".cfg" );
	if ( cfg.is_open() ) {
		std::string line;
		while ( std::getline( cfg, line ) ) {
			if ( !line.starts_with( kConfigEntry ) ) continue;
			try {
				scale_ = std::stof( line.substr( kConfigEntry.length() ) );
				if ( scale_ < 0.1f )
					scale_ = 0.1f;
				else if ( scale_ > 1.0f )
					scale_ = 1.0f;
			} catch ( ... ) {}
			break;
		}
		cfg.close();
	} else {
		std::ofstream cfg( PROJECT_NAME_C ".cfg" );
		if ( cfg.is_open() ) {
			cfg << kConfigEntry << scale_ << std::endl;
			cfg.close();
		}
	}

	svid = g_class.DirectX->onSetViewport += std::tuple{ this, &AsiPlugin::setViewport };

	scene.onBefore += std::tuple{ this, &AsiPlugin::sceneHook };
	scene.install( 0, 0, false );

	endRender.onBefore += std::tuple{ this, &AsiPlugin::endRenderHook };
	endRender.install( 0, 0, false );

	beginRender2D.onAfter += std::tuple{ this, &AsiPlugin::begin2D };
	beginRender2D.install( 0, 0, false );

	beginRenderMenu.onBefore += std::tuple{ this, &AsiPlugin::begin2D };
	beginRenderMenu.install( 0, 0, false );

	endRender2D.onBefore += std::tuple{ this, &AsiPlugin::end2D };
	endRender2D.install( 0, 0, false );

	// NOP bugged effects
	if ( scale_ != 1.0f ) {
		memsafe::nop( 0x53E175, 5 ); // CSkidmarks::Render
		memsafe::nop( 0x53E18E, 5 ); // CCoronas::Render
		memsafe::nop( 0x53E193, 18 ); // Fx_c::Render
		memsafe::nop( 0x726ADA, 5 ); // CBrightLights::Render
	}

	// fix textdraws in SAMP
	if ( !SAMP::Library() || ( !SAMP::isR1() && !SAMP::isR3() ) ) return;
	if ( SAMP::isR1() ) sampTextDraws.changeAddr( 0x9DAE7 );
	sampTextDraws.onBefore += std::tuple{ this, &AsiPlugin::begin2D };
	sampTextDraws.onAfter += std::tuple{ this, &AsiPlugin::end2D };
	sampTextDraws.install( 0, 0, false );
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	g_class.DirectX->onSetViewport -= svid;
}

void AsiPlugin::setViewport( const D3DVIEWPORT9 *vp ) {
	if ( !changeSize_ ) return;
	auto mutVP = const_cast<D3DVIEWPORT9 *>( vp );
	mutVP->Width *= scale_;
	mutVP->Height *= scale_;
}

void AsiPlugin::sceneHook() {
	endRenderHook();
	updateTextures();

	static auto &renderTarfet = *(LPDIRECT3DSURFACE9 *)0xC97C30;

	makeSmallTexture( renderTarfet );
	makeNormalTexture( renderTarfet );
	g_class.DirectX->d3d9_device()->SetRenderTarget( 0, renderTarfet );
}

void AsiPlugin::endRenderHook() {
	auto raster = RwCamera::get()->frameBuffer;
	D3DVIEWPORT9 vp;
	vp.MinZ = 0.0f;
	vp.MaxZ = 1.0f;
	vp.X = raster->nOffsetX;
	vp.Y = raster->nOffsetY;
	vp.Width = raster->width;
	vp.Height = raster->height;
	g_class.DirectX->d3d9_device()->SetViewport( &vp );
}

void AsiPlugin::begin2D() {
	changeSize_ = false;
	endRenderHook();
}

void AsiPlugin::end2D() {
	changeSize_ = true;
	auto raster = RwCamera::get()->frameBuffer;
	D3DVIEWPORT9 vp;
	vp.MinZ = 0.0f;
	vp.MaxZ = 1.0f;
	vp.X = raster->nOffsetX;
	vp.Y = raster->nOffsetY;
	vp.Width = raster->width * scale_;
	vp.Height = raster->height * scale_;
	g_class.DirectX->d3d9_device()->SetViewport( &vp );
}

void AsiPlugin::updateTextures() {
	if ( !txSmall_ )
		txSmall_ = g_class.DirectX->d3d9_createTexture( g_class.params.BackBufferWidth, g_class.params.BackBufferHeight );
	else if ( txSmall_->GetSize().x != g_class.params.BackBufferWidth || txSmall_->GetSize().y != g_class.params.BackBufferHeight )
		txSmall_->ReInit( g_class.params.BackBufferWidth, g_class.params.BackBufferHeight );

	if ( !txNormal_ )
		txNormal_ = g_class.DirectX->d3d9_createTexture( g_class.params.BackBufferWidth, g_class.params.BackBufferHeight );
	else if ( txNormal_->GetSize().x != g_class.params.BackBufferWidth || txNormal_->GetSize().y != g_class.params.BackBufferHeight )
		txNormal_->ReInit( g_class.params.BackBufferWidth, g_class.params.BackBufferHeight );
}

void AsiPlugin::makeSmallTexture( LPDIRECT3DSURFACE9 &surface ) {
	LPDIRECT3DSURFACE9 pTexSurface;
	txSmall_->GetTexture()->GetSurfaceLevel( 0, &pTexSurface );
	g_class.DirectX->d3d9_device()->StretchRect( surface, nullptr, pTexSurface, nullptr, D3DTEXF_NONE );
}

void AsiPlugin::makeNormalTexture( LPDIRECT3DSURFACE9 &surface ) {
	LPDIRECT3DSURFACE9 pTexSurface;
	txNormal_->GetTexture()->GetSurfaceLevel( 0, &pTexSurface );
	g_class.DirectX->d3d9_device()->SetRenderTarget( 0, pTexSurface );
	txSmall_->Render( 0, 0, g_class.params.BackBufferWidth / scale_, g_class.params.BackBufferHeight / scale_ );
	g_class.DirectX->d3d9_device()->StretchRect( pTexSurface, nullptr, surface, nullptr, D3DTEXF_NONE );
}
